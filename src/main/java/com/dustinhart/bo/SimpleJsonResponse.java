package com.dustinhart.bo;

public class SimpleJsonResponse {
	private boolean isSuccessful;
	private String msg;
	private Object data;
	
	public SimpleJsonResponse(boolean isSuccessful, String msg) {
		this(isSuccessful, msg, null);
	}
	
	public SimpleJsonResponse(boolean isSuccessful, String msg, Object data) {
		this.isSuccessful = isSuccessful;
		this.msg = msg;
		this.data = data;
	}

	public boolean isSuccessful() {
		return isSuccessful;
	}

	public void setSuccessful(boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
