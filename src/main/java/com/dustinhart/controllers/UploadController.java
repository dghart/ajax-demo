package com.dustinhart.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dustinhart.bo.SimpleJsonResponse;
import com.dustinhart.bo.User;

@Controller
public class UploadController {

	@RequestMapping(path = "/create_user", method = RequestMethod.POST)
	@ResponseBody
	public SimpleJsonResponse createUser(@RequestBody User user){
		// I don't think throwing an exception is the right way to validate 
		// input, but it demos the Exception handling Controller
		if(isBlank(user.getName())){
			throw new AjaxException("Please enter a name");
		}
		if(isBlank(user.getUsername())){
			throw new AjaxException("Please enter username");
		}
		if(isBlank(user.getPassword()) || user.getPassword().length() < 3){
			throw new AjaxException("Password must be at least 3 characters");
		}
		
		// TODO save user to database
		// If saving fails due to username in use or something, return 
		// a false SimpleJsonResponse instead
	
		return new SimpleJsonResponse(true, "New user was created!");
	}
	
	public static boolean isBlank(String str){
		return str == null || str.trim().isEmpty();
	}
	
	public static class AjaxException extends RuntimeException {
		public AjaxException(String msg){ super(msg); }
	}
	
	@ExceptionHandler(AjaxException.class)
	@ResponseBody
	public SimpleJsonResponse handleAjaxError(Exception exception){
		return new SimpleJsonResponse(false, exception.getMessage());
	}
}
